using System.Threading.Tasks;

namespace TestCaptcha
{
    public interface IReCaptchaService
    {
        Task<bool> VerifyAsync(string reCaptchaResponse);
    }
}