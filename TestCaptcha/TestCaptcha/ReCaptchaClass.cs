// using Newtonsoft.Json;
//
// namespace TestCaptcha
// {
//     public class ReCaptchaClass
//     {
//         public static string Validate(string EncodedResponse)
//         {
//             var client = new System.Net.WebClient();
//
//             //Use this site key in the HTML code your site serves to users 
//             // 6LfoQTcbAAAAAEd5aGWOlg-_TK0teGmmCP4Qgh5n
//
//             // Use this secret key for communication between your site and reCAPTCHA
//             // 6LfoQTcbAAAAAF52AbgQpSloYvtyV-OyDezNqW-V
//             
//             string PrivateKey = "6LcH-v8SerfgAPlLLffghrITSL9xM7XLrz8aeory";
//
//             var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse));
//
//             var captchaResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ReCaptchaClass>(GoogleReply);
//
//             return captchaResponse.Success.ToLower();
//         }
//
//         [JsonProperty("success")]
//         public string Success
//         {
//             get { return m_Success; }
//             set { m_Success = value; }
//         }
//
//         private string m_Success;
//         [JsonProperty("error-codes")]
//         public List<string> ErrorCodes
//         {
//             get { return m_ErrorCodes; }
//             set { m_ErrorCodes = value; }
//         }
//
//
//         private List<string> m_ErrorCodes;
//     }
// }