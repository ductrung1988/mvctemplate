// using System;
// using System.Globalization;
// using System.Text;
//
// namespace TestCaptcha
// {
//     
//     public class RecaptchaV3HiddenInputBase
//     {
//         private RecaptchaV3HiddenInputBase.ToStringInstanceHelper toStringHelperField = new RecaptchaV3HiddenInputBase.ToStringInstanceHelper();
//
//         public class ToStringInstanceHelper
//         {
//             private IFormatProvider formatProviderField = (IFormatProvider) CultureInfo.InvariantCulture;
//
//             public IFormatProvider FormatProvider
//             {
//                 get => this.formatProviderField;
//                 set
//                 {
//                     if (value == null)
//                         return;
//                     this.formatProviderField = value;
//                 }
//             }
//         }
//     }
//     
//
//     public class RenderHtml
//     {
//         private string currentIndentField = "";
//         private bool endsWithNewline;
//         private StringBuilder generationEnvironmentField;
//         public RecaptchaV3HiddenInputBase.ToStringInstanceHelper ToStringHelper => this.toStringHelperField;
//         protected StringBuilder GenerationEnvironment
//         {
//             get
//             {
//                 if (this.generationEnvironmentField == null)
//                     this.generationEnvironmentField = new StringBuilder();
//                 return this.generationEnvironmentField;
//             }
//             set => this.generationEnvironmentField = value;
//         }
//         public void Write(string textToAppend)
//         {
//             if (string.IsNullOrEmpty(textToAppend))
//                 return;
//             if (this.GenerationEnvironment.Length == 0 || this.endsWithNewline)
//             {
//                 this.GenerationEnvironment.Append(this.currentIndentField);
//                 this.endsWithNewline = false;
//             }
//             if (textToAppend.EndsWith(Environment.NewLine, StringComparison.CurrentCulture))
//                 this.endsWithNewline = true;
//             if (this.currentIndentField.Length == 0)
//             {
//                 this.GenerationEnvironment.Append(textToAppend);
//             }
//             else
//             {
//                 textToAppend = textToAppend.Replace(Environment.NewLine, Environment.NewLine + this.currentIndentField);
//                 if (this.endsWithNewline)
//                     this.GenerationEnvironment.Append(textToAppend, 0, textToAppend.Length - this.currentIndentField.Length);
//                 else
//                     this.GenerationEnvironment.Append(textToAppend);
//             }
//         }
//         public virtual string TransformText()
//         {
//             this.Write("\r\n<input id=\"");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Id));
//             this.Write("\" name=\"g-recaptcha-response\" type=\"hidden\" value=\"\" />\r\n<script ");
//             if (!string.IsNullOrEmpty(this.Model.Settings.ContentSecurityPolicy))
//             {
//                 this.Write("script-src=\"");
//                 this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Settings.ContentSecurityPolicy));
//                 this.Write("\"");
//             }
//             this.Write(" ");
//             if (!string.IsNullOrEmpty(this.Model.Settings.ContentSecurityPolicy))
//             {
//                 this.Write("frame-src=\"");
//                 this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Settings.ContentSecurityPolicy));
//                 this.Write("\"");
//             }
//             this.Write(" src=\"https://");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Settings.Site));
//             this.Write("/recaptcha/api.js?render=");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Settings.SiteKey));
//             this.Write("&hl=");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Language));
//             this.Write("\" ");
//             if (this.Model.IsAsync)
//                 this.Write("async defer");
//             this.Write(" ></script>\r\n<script>\r\n\tif (typeof grecaptcha !== 'undefined') {\r\n\t\tgrecaptcha.ready(function () {\r\n\t\t\tgrecaptcha.execute('");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Settings.SiteKey));
//             this.Write("', { 'action': '");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Action));
//             this.Write("' }).then(function (token) {\r\n\t\t\t\tdocument.getElementById('");
//             this.Write(this.ToStringHelper.ToStringWithCulture((object) this.Model.Id));
//             this.Write("').value = token;\r\n\t\t\t});\r\n\t\t});\r\n\t}\r\n</script>");
//             return this.GenerationEnvironment.ToString();
//         }
//     }
// }