using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AspNetCore.ReCaptcha;
using Microsoft.AspNetCore.Mvc;
using TestCaptcha.Models;

namespace TestCaptcha.Controllers
{
    [ApiController]
    [Route("public/signup")]
    public class SignUp : ControllerBase
    {
        IGoogleRecaptchaV3Service _gService { get; set; }
        public SignUp(IGoogleRecaptchaV3Service gService)
        {
            _gService = gService;
        }
        
        [ValidateReCaptcha]
        [HttpPost]
        public async Task<IActionResult> Post([FromQuery] SignUpModel SignUpData)
        {
            Console.WriteLine("Start Post");
            // GRequestModel rm = new GRequestModel(SignUpData.RecaptchaToken,
            //     HttpContext.Connection.RemoteIpAddress.ToString());
            //
            // _gService.InitializeRequest(rm);
            //
            // if (!await _gService.Execute())
            // {
            //     //return error codes string.
            //     return Ok(_gService.Response);
            // }

            //call Business layer

            //return result
            //TODO: possibly return de-serialized Response from Google API.
            return Ok("Server-side Google reCaptcha validation successfull!");
        }
        
        // User
    }
}