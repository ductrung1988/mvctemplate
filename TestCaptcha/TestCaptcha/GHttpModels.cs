using System;
using System.Runtime.Serialization;

namespace TestCaptcha
{
    public class GRequestModel
    {
        public string path { get; set; }
        public string secret { get; set; }
        public string response { get; set; }
        public string remoteip { get; set; }

        public GRequestModel(string res, string remip)
        {
            response = res;
            remoteip = remip;
            secret = Startup.Configuration["GoogleRecaptchaV3:Secret"];
            path = Startup.Configuration["GoogleRecaptchaV3:ApiUrl"];
            if(String.IsNullOrWhiteSpace(secret) || String.IsNullOrWhiteSpace(path))
            {
                //Invoke logger
                throw new Exception("Invalid 'Secret' or 'Path' properties in appsettings.json. Parent: GoogleRecaptchaV3.");
            }
        }
        
    }
    
    [DataContract]
    public class GResponseModel
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public string challenge_ts { get; set; }
        [DataMember]
        public string hostname { get; set; }
        [DataMember]
        public decimal score { get; set; }
        [DataMember]
        public string action { get; set; }

        //Could create a child object for 
        //error-codes
        [DataMember(Name = "error-codes")]
        public string[] error_codes { get; set; }
    }
}