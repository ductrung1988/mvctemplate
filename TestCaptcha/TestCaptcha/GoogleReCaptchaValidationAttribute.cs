using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Web;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace TestCaptcha
{
    public class GoogleReCaptchaValidationAttribute : ValidationAttribute
    {

        public GRequestModel Request { get; set; }
        public GResponseModel Response { get; set; }

        #region No use

        // protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        // {
        //     
        //     Console.WriteLine($"Retrieved Response");
        //     Lazy<ValidationResult> errorResult = new Lazy<ValidationResult>(() => new ValidationResult("Google reCAPTCHA validation failed", new String[] { validationContext.MemberName }));  
        //
        //     if (value == null || String.IsNullOrWhiteSpace( value.ToString()))   
        //     {  
        //         return errorResult.Value;  
        //     }  
        //
        //     
        //     IConfiguration configuration = (IConfiguration)validationContext.GetService(typeof(IConfiguration));  
        //     String reCaptchResponse = value.ToString();  
        //     String reCaptchaSecret = configuration.GetValue<String>("GoogleReCaptcha:SecretKey");  
        //       
        //
        //     HttpClient httpClient = new HttpClient();  
        //     var httpResponse = httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret={reCaptchaSecret}&response={reCaptchResponse}").Result;  
        //     Console.WriteLine($"Retrieved Response: {httpResponse}");
        //     if (httpResponse.StatusCode != HttpStatusCode.OK)  
        //     {  
        //         return errorResult.Value;  
        //     }  
        //
        //     String jsonResponse = httpResponse.Content.ReadAsStringAsync().Result;  
        //     dynamic jsonData = JObject.Parse(jsonResponse);  
        //     
        //     Console.WriteLine($"Retrieved Response: {jsonData}");
        //     if (jsonData.success != true.ToString().ToLower())  
        //     {  
        //         return errorResult.Value;  
        //     }  
        //
        //     return ValidationResult.Success;  
        //
        // }  

        #endregion
       
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Console.WriteLine($"Start Validate");
            
            Lazy<ValidationResult> errorResult = new Lazy<ValidationResult>(() =>
                new ValidationResult("Google reCAPTCHA validation failed",
                    new String[] {validationContext.MemberName}));

            if (value == null || String.IsNullOrWhiteSpace(value.ToString()))
            {
                return errorResult.Value;
            }

            Request = new GRequestModel(value.ToString(), string.Empty);
            string builtUrl = Request.path + '?' +
                              HttpUtility.UrlPathEncode($"secret={Request.secret}&response={Request.response}");
            
            //send request.
            HttpClient httpClient = new HttpClient();  
            HttpResponseMessage response = httpClient.GetAsync(builtUrl).Result;
            response.EnsureSuccessStatusCode();

            //read response
            byte[] res = response.Content.ReadAsByteArrayAsync().Result;

            string logres =  response.Content.ReadAsStringAsync().Result;
            Console.WriteLine($"Retrieved Response: {logres}");
            
            //Serialize into GReponse type
            using (MemoryStream ms = new MemoryStream(res))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(GResponseModel));
                Response = (GResponseModel) serializer.ReadObject(ms);
            }
            
            if (Response is {success: true} && Response.score >= 0.5M)
            {
                return ValidationResult.Success;
            }
            
            return errorResult.Value;
        }
    }
}