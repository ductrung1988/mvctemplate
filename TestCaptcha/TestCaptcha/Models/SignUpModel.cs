using System;
using System.ComponentModel.DataAnnotations;

namespace TestCaptcha.Models
{
    public class SignUpModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string RecaptchaToken { get; set; }
    }
}