using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace TestCaptcha
{
    public abstract class GoogleReCaptchaModelBase  
    {  
        [Required]  
        [GoogleReCaptchaValidation]  
        [BindProperty(Name = "RecaptchaToken")]  
        public String GoogleReCaptchaResponse { get; set; }  
    }  
}